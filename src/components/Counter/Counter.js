import React, {Component} from 'react';
import {connect} from 'react-redux';

import './Counter.css';

const btns = [ "1", "2", "3", "C", "4", "5", "6", "/", "7", "8", "9", "*", "0", ".", "+", "-", "=" ];

class Counter extends Component {

  render() {
    return (
      <div className="Counter">
        <div className="input">
          <input type="text" defaultValue={this.props.value} />
          <div className="btn">
            {btns.map((item, key) => {
              if(item === "C"){
                return(
                  <button onClick={this.props.clear} key={key}>{item}</button>
                )
              } else if(item === "="){
                return(
                  <button onClick={this.props.equal} key={key}>{item}</button>
                )
              } else {
                return(
                  <button onClick={this.props.addElem} key={key}>{item}</button>
                )
              }
            })}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    value: state.value,
  };
};

const mapDispatchToProps = (dispatch) => {
  return{
    addElem: event => {
      dispatch({
        type: 'ADD_ELEM',
        value: event.target.innerText}
        )},
    clear: () => {
      dispatch({
        type: 'CLEAR',
      })
    },
    equal: () => {
      dispatch({
        type: 'EQUAL',
      })
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Counter);